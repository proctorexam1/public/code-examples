import json
import hashlib
import hmac
import base64
import requests
import time

API_TOKEN='your_token'
SECRET_KEY='secret_key'
DOMAIN='<your_subdomain>.proctorexam.com'

def create_signature(data):
    """
    Creates the signature

    - **parameters**, **type**::
        :data Dict: Data from where the base string will be created
    """
    base_string = create_base_string( data )
    digest = hmac.new( bytes( SECRET_KEY, 'utf-8' ),
                     bytes( '', 'utf-8' ),
                     hashlib.sha256 )

    digest.update( bytes( base_string, 'utf-8' ) )

    return digest.hexdigest()

def create_base_string(data):
    """
    Creates the base string from which the signature will be generated

    - **parameters**, **type**::
        :data Dict: Data from where the base string will be created
    """
    values = []
    key_sorted_data = list(data.items())
    key_sorted_data.sort()

    for key, value in key_sorted_data:
        if type( value ) is list:
            value = json.dumps( value )

        values.append( str( key ) + "=" + str( value ) )

    return "?".join( values )

def create_exam():
    # Generating and signing the params
    params = {
        "name": 'Test',
        "type": 'record_review',
        "nonce": round(time.time()),
        "timestamp": round(time.time() * 1000)
    }

    headers = {
        'Content-Type': 'application/json',
        'Authorization': f'Token token={API_TOKEN}',
        'Accept': 'application/vnd.procwise.v3',
    }

    signature = create_signature(params)
    params["signature"] = signature
    response = requests.post(f'https://{DOMAIN}/api/v3/exams', json=params, headers=headers, verify=False)
    print(response)

create_exam()
