require 'net/http'
require 'openssl'
require 'json'

API_TOKEN='your_token'
SECRET_KEY='secret_key'
DOMAIN='<your_subdomain>.proctorexam.com'

# Creates a secret from the params object
def generate_signature(params)
  signature = OpenSSL::HMAC.hexdigest(OpenSSL::Digest.new('sha256'), SECRET_KEY, params.keys.sort.map{ |key| "#{key}=#{params[key]}" }.join('?'))

  return signature
end

def create_exam()
  headers = {
    'Content-Type' => 'application/json',
    'Authorization' => "Token token=#{API_TOKEN}",
    'Accept' => 'application/vnd.procwise.v3'
  }

  uri = URI.parse("https://#{DOMAIN}/api/v3/exams")

  # Generating and signing the params
  params = {
    name: 'Test',
    type: 'record_review',
    nonce: (Time.now.to_f).to_i,
    timestamp: (Time.now.to_f * 1000).to_i
  }

  signature = generate_signature(params)
  params['signature'] = signature

  # Creating the request
  req = Net::HTTP::Post.new(uri, headers)
  req.body = params.to_json
  http = Net::HTTP.new(uri.hostname, uri.port)
  # http.verify_mode = OpenSSL::SSL::VERIFY_NONE
  http.use_ssl = true
  res = http.request(req)
  
  p JSON.parse res.body
end
