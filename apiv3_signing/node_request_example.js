const crypto = require('crypto');
const https = require('https');

const API_TOKEN='your_token';
const SECRET_KEY='secret_key';
const DOMAIN='<your_subdomain>.proctorexam.com'

/**
 * Will generate a signature
 * @param {Object} params
 */
function generateSignature(params) {
  const paramsString = generateParamsString(params);
  return crypto.createHmac('sha256', SECRET_KEY).update(paramsString).digest('hex');
}

/**
 * Returns string with the format key=value? from the params object
 * @param {Object} params
 */
function generateParamsString(params) {
  let string = "";
  const keys = Object.keys(params);
  keys.sort();

  for (let key of keys) {
    let value = typeof(params[key]) == "object" ? JSON.stringify(params[key]) : params[key];

    string += key + "=" + value + "?";
  }

  return Buffer.from(string.slice(0, -1)).toString("utf-8");
}

function createExam() {
  // Generating and signing the params
  const params = {
    name: 'Test',
    type: 'record_review',
    nonce: Math.round(Date.now() / 1000),
    timestamp: Date.now()
  };

  signature = generateSignature(params);
  params.signature = signature;

  const stringifiedParams = JSON.stringify(params);

  const options = {
    hostname: DOMAIN,
    port: 443,
    path: '/api/v3/exams',
    method: 'POST',
    // rejectUnauthorized: false,
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Token token=${API_TOKEN}`,
      'Accept': 'application/vnd.procwise.v3',
      'Content-Length': stringifiedParams.length
    }
  }

  const req = https.request(options, (res) => {
    console.log('Status code:', res.statusCode);

    res.on('data', (data) => {
      process.stdout.write(data);
    });
  });

  req.on('error', error => {
    console.error(error)
  })

  req.write(stringifiedParams);
  req.end();
}

createExam();