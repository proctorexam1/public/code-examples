<?php

$API_TOKEN = 'your_token';
$SECRET_KEY = 'secret_key';
$DOMAIN = '<your_subdomain>.proctorexam.com';

function create_signature($data, $secret_key) {
    // Create the base string
    $base_string = create_base_string($data);

    // Generate HMAC SHA256 signature
    $hash = hash_hmac('sha256', $base_string, $secret_key, true);

    // Return the hexadecimal representation of the hash
    return bin2hex($hash);
}

function create_base_string($data) {
    // Sort data by keys
    ksort($data);

    // Build the base string
    $values = [];
    foreach ($data as $key => $value) {
        if (is_array($value)) {
            $value = json_encode($value);
        }
        $values[] = $key . "=" . $value;
    }

    return implode("?", $values);
}

function create_exam($api_token, $secret_key, $domain) {
    // Generate and sign the parameters
    $params = [
        "name" => 'Test',
        "type" => 'record_review',
        "nonce" => time(),
        "timestamp" => round(microtime(true) * 1000)
    ];

    $headers = [
        'Content-Type: application/json',
        'Authorization: Token token=' . $api_token,
        'Accept: application/vnd.procwise.v3'
    ];

    $signature = create_signature($params, $secret_key);
    $params["signature"] = $signature;

    // Initialize cURL session
    $ch = curl_init();

    // Set cURL options
    curl_setopt($ch, CURLOPT_URL, "https://$domain/api/v3/exams");
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params));
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // Not recommended for production

    // Execute the request
    $response = curl_exec($ch);

    // Check for errors
    if (curl_errno($ch)) {
        echo 'Request Error:' . curl_error($ch);
    } else {
        echo $response;
    }

    // Close the cURL session
    curl_close($ch);
}

create_exam($API_TOKEN, $SECRET_KEY, $DOMAIN);

?>