require 'net/http'
require 'uri'
require 'json'
require 'openssl'

API_TOKEN='your_token'
SECRET_KEY='secret_key'
DOMAIN='<your_subdomain>.proctorexam.com'

# Creates a secret from the params object
def generate_signature(params)
  signature = OpenSSL::HMAC.hexdigest(OpenSSL::Digest.new('sha256'), SECRET_KEY, params.keys.sort.map{ |key| "#{key}=#{params[key]}" }.join('?'))

  return signature
end

def fetch_students
  params = {
    filter: {
      status: { nin: ["registered", "exam_not_started", "exam_finished"] },
      open_time: { gte: "2021-09-01T00:00:00Z", lte: "2025-02-31T00:00:00Z" },
      archived: { eq: true }
    }.to_json,
    sort: [
      { exam_id: "desc" },
      { created_at: "asc" }
    ].to_json,
    timestamp: (Time.now.to_f * 1000).to_i,
    nonce: Time.now.to_i
  }

  params[:signature] = generate_signature(params)
  
  uri = URI("https://#{DOMAIN}/api/v3/student_sessions?" + URI.encode_www_form(params))
  request = Net::HTTP::Get.new(uri)
  request['Accept'] = 'application/vnd.procwise.v3'
  request['Content-Type'] = 'application/json'
  request['Authorization'] = "Token token=#{API_TOKEN}"

  response = Net::HTTP.start(uri.host, uri.port, use_ssl: true) do |http|
    http.request(request)
  end

  p JSON.parse response.body
end

fetch_students
