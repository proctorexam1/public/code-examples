import requests
import json
import hashlib
import hmac
import time
from urllib.parse import urlencode

API_TOKEN = 'your_token'
SECRET_KEY = 'your_secret'
DOMAIN = '<your_subdomain>.proctorexam.com'

def create_signature(data):
    """
    Creates the signature

    - **parameters**, **type**::
        :data Dict: Data from where the base string will be created
    """
    base_string = create_base_string( data )
    digest = hmac.new( bytes( SECRET_KEY, 'utf-8' ),
                     bytes( '', 'utf-8' ),
                     hashlib.sha256 )

    digest.update( bytes( base_string, 'utf-8' ) )

    return digest.hexdigest()

def create_base_string(data):
    """
    Creates the base string from which the signature will be generated

    - **parameters**, **type**::
        :data Dict: Data from where the base string will be created
    """
    values = []
    key_sorted_data = list(data.items())
    key_sorted_data.sort()

    for key, value in key_sorted_data:
        if type( value ) is list:
            value = json.dumps( value )

        values.append( str( key ) + "=" + str( value ) )

    return "?".join( values )

def fetch_students():
    
    params = {
        "filter": json.dumps({
            "status": {"nin": ["registered", "exam_not_started", "exam_finished"]},
            "open_time": {"gte": "2021-09-01T00:00:00Z", "lte": "2025-02-31T00:00:00Z"},
            "archived": {"eq": True}
        }),
        "sort": json.dumps([
            {"exam_id": "desc"},
            {"created_at": "asc"}
        ]),
        "timestamp": int(time.time() * 1000),
        "nonce": int(time.time())
    }
    
    params["signature"] = create_signature(params)
    
    url = f"https://{DOMAIN}/api/v3/student_sessions?" + urlencode(params)
    headers = {
        "Accept": "application/vnd.procwise.v3",
        "Content-Type": "application/json",
        "Authorization": f"Token token={API_TOKEN}"
    }
    
    response = requests.get(url, headers=headers)
    print(response.text)

fetch_students()

