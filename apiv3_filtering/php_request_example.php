<?php

$API_TOKEN = 'your_token';
$SECRET_KEY = 'secret_key';
$DOMAIN = '<your_subdomain>.proctorexam.com';

function create_signature($data, $secret_key) {
    // Create the base string
    $base_string = create_base_string($data);

    // Generate HMAC SHA256 signature
    $hash = hash_hmac('sha256', $base_string, $secret_key, true);

    // Return the hexadecimal representation of the hash
    return bin2hex($hash);
}

function create_base_string($data) {
    // Sort data by keys
    ksort($data);

    // Build the base string
    $values = [];
    foreach ($data as $key => $value) {
        if (is_array($value)) {
            $value = json_encode($value);
        }
        $values[] = $key . "=" . $value;
    }

    return implode("?", $values);
}

function fetchStudents($apiToken, $secretKey, $domain) {
    $params = [
        "filter" => json_encode([
            "status" => ["nin" => ["registered", "exam_not_started", "exam_finished"]],
            "open_time" => ["gte" => "2021-09-01T00:00:00Z", "lte" => "2025-02-31T00:00:00Z"],
            "archived" => ["eq" => true]
        ]),
        "sort" => json_encode([
            ["exam_id" => "desc"],
            ["created_at" => "asc"]
        ]),
        "timestamp" => time() * 1000,
        "nonce" => time()
    ];
    
    $params['signature'] = create_signature($params, $secretKey);
    
    $url = "https://$domain/api/v3/student_sessions?" . http_build_query($params);
    
    $options = [
        'http' => [
            'method' => 'GET',
            'header' => [
                'Accept: application/vnd.procwise.v3',
                'Content-Type: application/json',
                'Authorization: Token token=' . $apiToken
            ]
        ]
    ];
    
    $context = stream_context_create($options);
    $response = file_get_contents($url, false, $context);
    
    echo $response;
}

fetchStudents($API_TOKEN, $SECRET_KEY, $DOMAIN);
