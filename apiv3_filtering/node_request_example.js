import crypto from 'crypto';

const API_TOKEN='your_token';
const SECRET_KEY='your_secret';
const DOMAIN='your_domain.proctorexam.com';

/**
 * Will generate a signature
 * @param {Object} params
 */
function generateSignature(params) {
  const paramsString = generateParamsString(params);
  return crypto.createHmac('sha256', SECRET_KEY).update(paramsString).digest('hex');
}

/**
 * Returns string with the format key=value? from the params object
 * @param {Object} params
 */
function generateParamsString(params) {
  let string = "";
  const keys = Object.keys(params);
  keys.sort();

  for (let key of keys) {
    let value = typeof(params[key]) == "object" ? JSON.stringify(params[key]) : params[key];

    string += key + "=" + value + "?";
  }

  return Buffer.from(string.slice(0, -1)).toString("utf-8");
}

/**
 * Fetch and log filtered and sorted students
 */
async function fetchStudents() {
  const params = {
    filter: JSON.stringify({
	    "status": { "nin": ["registered", "exam_not_started", "exam_finished"]},
	    "open_time": { "gte": "2021-09-01T00:00:00Z", "lte": "2025-02-31T00:00:00Z" },
	    "archived": { "eq": true }
    }),
    sort: JSON.stringify([{ "exam_id": "desc" }, { "created_at": "asc" }]),
    timestamp: Date.now(),
    nonce: Math.round(Date.now() / 1000)
  };

  const signature = generateSignature(params);
  params.signature = signature;

  const options = {
    hostname: DOMAIN,
    port: 443,
    path: '/api/v3/student_sessions',
    method: 'GET',
    // rejectUnauthorized: false,
    headers: {
      'Accept': 'application/vnd.procwise.v3',
      'Content-Type': 'application/json',
      'Authorization': `Token token=${API_TOKEN}`
    }
  }
  const res = await fetch(`https://${options.hostname}${options.path}?${new URLSearchParams(params).toString()}`, options).then(res => res.json());
  console.log(res);
}

fetchStudents();
