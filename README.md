# ProctorExam API v3

This repository contains some useful code examples that can be used in conjunction with the [ProctorExam APIv3](https://docs.proctorexam.com/v3/apidoc.html).

<!-- TOC -->
* [ProctorExam API v3](#proctorexam-api-v3)
  * [Signing APIv3 requests](#signing-apiv3-requests)
  * [Creating the student session links](#creating-the-student-session-links)
  * [Creating Exam instructions for students](#creating-exam-instructions-for-students)
  * [Pagination](#pagination)
  * [Sorting and filtering](#sorting-and-filtering)
  * [Callbacks](#callbacks)
<!-- TOC -->

## Signing APIv3 requests

1. You'll need a user that makes the API requests, we'll call it the API user:
   - It's recommended to create a special user for the API, so that the application making the API calls isn't tied to a particular member of your organisation,
   leading to problems when that person leaves. For example, you can use a group email address for the API user.
   - The API user should be a `superuser` if you have a Premium environment, so that it can manage resources across institutes
   - You may also use an API user with the role `administrator` if the resources should be limited to one institute. 
   - The API user will have the same rights as a normal user, i.e. an `administrator` will **not** be able to manage resources in a 
   different institute.
2. Login as the API user and get your API token and secret from your ProctorExam platform **Settings > Advanced Settings**.
3. Place the **token** in the Authorization request header (the token is used to identify your user)
    ```json
    "headers": {
        "Accept": "application/vnd.procwise.v3",
        "Content-Type": "application/json",
        "Authorization": "Token token=zL8NGW8tmjhZvEDjo8naqA"
        }
    ```
4. Generating the signature
    - Create a string from the params object with the format `key=value` separated by `?` and ordered alphabetically by `key`. Note:
    Every required parameter from our documentation needs to be included. This often includes the `id` of the object requested, even if the `id`
    is already included in the path.
    - Create a `HMAC sha256` hash from the string built in the previous step and using the secret key for your user
    - Add the signature to the params with the key `signature`.
5. Refer to the examples in this repository:
    - [NodeJS](apiv3_signing/node_request_example.js)
    - [Python](apiv3_signing/python_request_example.py)
    - [Ruby](apiv3_signing/ruby_request_example.rb)
    - [PHP](apiv3_signing/php_request_example.php)

## Creating the student session links

If you're creating the student sessions via the API, you might need to build the links to the session itself that the exam-taker will use to perform
the system check or take the exam. This is useful if you're disabling the automatic email sending on the ProctorExam platform and sending the emails
yourself.

1. Get the desired student session from the platform: [`GET /student_sessions/:id`](https://docs.proctorexam.com/v3/apidoc/student_sessions/show.html).
Note: The same information is also received when you create the student session as a response to [`POST /exams/:id/student_sessions`](https://docs.proctorexam.com/v3/apidoc/student_sessions/create.html)
2. Extract the student session **token** from the response, the key name is `token`
3. Build the link:
    - For the system check (check requirements): `https://<environment>.proctorexam.com/check_requirements/<token>`
    - For the exam room: `https://<environment>.proctorexam.com/student_sessions/<token>`

## Creating Exam instructions for students

Note that when you create an Exam with the API, it will be in `draft` status until you create the exam instructions for the 
students. 

Here's an example request to create an exam:
```curl
curl --location 'https://staging.proctorexam.com/api/v3/exams?signature=d32216390c65e82c400d5cd18606f3e90e41dac14383ebd9e55d86491755320b&timestamp=1737475312034&nonce=1737475312' \
--header 'Accept: application/vnd.procwise.v3' \
--header 'Content-Type: application/json' \
--header 'Authorization: Token token=...' \
--data '{
    "institute_id": 5529,
    "name": "Exam for API Testing",
    "type": "record_review",
    "for_reviewing": true,
    "duration_minutes": 60,
    "use_duration": true,
    "upload_answers": true
}'
```
And here's the response:
```json
{
  "exam": {
    "id": 22512,
    "institute_id": 5529,
    "name": "Exam for API Testing",
    "terms": null,
    "created_at": "2025-01-21T17:00:47.318+01:00",
    "updated_at": "2025-01-21T17:00:47.318+01:00",
    "duration_minutes": 60,
    "clipboard": null,
    "start_time": "2025-01-21T17:00:47.311+01:00",
    "mode": "01010000",
    "upload_answers": true,
    "token": "...",
    "user_id": 26,
    "end_time": null,
    "restrictions": "[[false, [\"\"]], [false, [\"\"]], [false, [\"\"]], [false, [\"\"]], [false, [\"\"]], [false, [\"\"]], [true, [\"\"]]]",
    "published": false,
    "status": "draft",
    "for_reviewing": true,
    "uploaded_exam_documents_file_name": null,
    "uploaded_exam_documents_content_type": null,
    "uploaded_exam_documents_file_size": null,
    "uploaded_exam_documents_updated_at": null,
    "timezone": null,
    "use_duration": true,
    "global_proctoring": false,
    "global_reviewing": false,
    "exam_language": "en-GB",
    "archived": false,
    "for_ai_reviewing": false,
    "deleted": false,
    "student_sessions_count": 0,
    "web_cam": true,
    "mobile_cam": false,
    "screen_share": true,
    "live_proctoring": false
  }
}
```
As you can see, the student instructions aren't included and the exam `status` is `draft`. In order to create the exam instructions, you need to create a `document`. 

The `document` has one relevant field, the `exam_content` field, which will contain the instruction for the students. In many cases, such 
instructions would also contain some files, e.g. a PDF that the students can download with more details of the exam or even the 
complete exam questions. Such files should be uploaded as `attachment` before creating your `document`. 

Here's an example query to upload an `attachment` to the above exam (`exam_id = 22512`):
```curl
curl --location 'https://staging.proctorexam.com/api/v3/exams/22512/attachments?signature=652dee8c48b4e0aabefb7d6e2369a61c699dfef4519ebc5e13326b5bb259d292&nonce=1737540417&timestamp=1737540416502&exam_id=22512' \
--header 'Accept: application/vnd.procwise.v3' \
--header 'Content-Type: application/json' \
--header 'Authorization: Token token=...' \
--form 'document=@"/Users/someuser/Documents/Exam\ Instructions.pdf"'
```

And the response:
```json
{
  "attachment": {
    "id": 420,
    "created_at": "2025-01-22T11:06:56.873+01:00",
    "updated_at": "2025-01-22T11:06:56.873+01:00",
    "document_file_name": "Exam_instructions.pdf",
    "document_content_type": "application/pdf",
    "document_file_size": 109298,
    "document_updated_at": "2025-01-22T11:06:56.872+01:00",
    "exam_id": 22512,
    "placeholder": "{1737540416_Exam_instructions'.pdf}"
  }
}
```
Now we can use this `attachment` in the student's exam instructions, knowing its `placeholder` is `{1737540416_Exam_instructions.pdf}`:
```curl
curl --location 'https://staging.proctorexam.com/api/v3/exams/22512/documents?signature=bb2c402dc27e3e42e6044a3d2d56efb425417d3f9851b45fe3ff9cbc785e8123&nonce=1737541111&timestamp=1737541110827' \
--header 'Accept: application/vnd.procwise.v3' \
--header 'Content-Type: application/json' \
--header 'Authorization: Token token=...' \
--data '{
"exam_id": 22512,
"exam_content": "<h2>Instructions for your exam</h2>\n<p>Remember that once you start your exam, you are not allowed to leave the desk. Read the exam instructions carefully. We expect you to write the entire exam answers in a plain Word document.&nbsp;</p>\n<p>Once finished, please upload your document using the upload button below these intructions.</p>\n<p>Please download the exam questions here: {1737540416_Exam_instructions.pdf}.&nbsp;</p>\n<p>It is important that you delete that file before finishing your exam! You are recorded during the entire duration of the exam and will be reminded to delete this file before closing the exam session.</p>"
}'
```
As you can see, you can use some HTML tags to format your instructions. Please note that only formatting tags will be allowed,
any script, style or iframe tags, but also style attributes will be stripped.

The response shows the new `document`:
```json
{
  "document": {
    "id": 21698,
    "exam_content": "\u003ch2\u003eInstructions for your exam\u003c/h2\u003e\n\u003cp\u003eRemember that once you start your exam, you are not allowed to leave the desk. Read the exam instructions carefully. We expect you to write the entire exam answers in a plain Word document.\u0026nbsp;\u003c/p\u003e\n\u003cp\u003eOnce finished, please upload your document using the upload button below these intructions.\u003c/p\u003e\n\u003cp\u003ePlease download the exam questions here: {1737540416_Exam_instructions.pdf}.\u0026nbsp;\u003c/p\u003e\n\u003cp\u003eIt is important that you delete that file before finishing your exam! You are recorded during the entire duration of the exam and will be reminded to delete this file before closing the exam session.\u003c/p\u003e",
    "created_at": "2025-01-22T11:18:31.028+01:00",
    "updated_at": "2025-01-22T11:18:31.028+01:00",
    "exam_id": 22512,
    "file_file_name": null,
    "file_content_type": null,
    "file_file_size": null,
    "file_updated_at": null,
    "file_processing": false
  }
}
```
After fetching the exam using a GET request, we can see that the exam now has `status` set to `open`:
```json
{
  "exam": {
    "id": 22512,
    "institute_id": 5529,
    "name": "Exam for API Testing",
    "terms": null,
    "created_at": "2025-01-21T17:00:47.318+01:00",
    "updated_at": "2025-01-22T11:15:51.904+01:00",
    "duration_minutes": 60,
    /*...*/
    "start_time": "2025-01-21T17:00:47.311+01:00",
    "mode": "01010000",
    "upload_answers": true,
    /*...*/
    "status": "open",
    "for_reviewing": true,
    /*...*/
  }
}
```
    
## Pagination

Many of our APIs return paginated results, with a default page length of 300 items. In order to receive all results, you should read the response headers for pagination and
set the correct page number for your requests.

1. Fetch a paginated list by adding the `page` parameter to your queries, e.g. `GET /student_sessions?page=1` or `GET /exams/:id/student_sessions?page=3`
2. Parse the 2 pagination HTTP headers `X-Pagination-Page-Count` (total number of pages) and `X-Pagination-Page` (current page number). If the page count is greater than
the current page number, there are more pages available.
3. Fetch the next page by setting `page` query parameter to `X-Pagination-Page + 1`, e.g. `GET /student_sessions?page=2`
4. Continue querying the API until you reach the last page: `X-Pagination-Page == X-Pagination-Page-Count`
5. Use the `limit` parameter to specify the number of items per page. For example, if you are displaying the results in a front-end that also paginates the results with
50 items per page, you would use `GET /student_sessions?page=2&limit=50`

There are 2 other HTTP headers that give information about the pagination of the response: `X-Pagination-Item-Count` tells you the total number of items that can be retrieved
via the API; `X-Pagination-Limit` tells you the number of items per page.

The default number of items per page is 300. You can change it with the `limit` parameter to a lower value (not higher). If you request a page with `limit > 300`, the query
will still return at most 300 items (and the `X-Pagination-Limit` will be 300).

Note: If you request a page `N > X-Pagination-Page-Count`, the response will return an empty list, with `X-Pagination-Page = N`.

## Sorting and filtering

You can sort and filter the results of the API queries by using the `sort` and `filter` parameters. The `sort` parameter is used to sort the results by a specific column. It's formatted as a stringified JSON array with the column name and the direction of the sort (`asc` or `desc`). The `filter` parameter is used to filter the results by a specific column. It's formatted as a stringified JSON object with the column name as the key and the value is an object with the operator and the value to filter by. The available operators are `eq`, `ne`, `in`, `nin`, `gt`, `gte`, `lt`, `lte`.

Examples in different languages can be found in the following files: [NodeJS](apiv3_filtering/node_request_example.js), [Python](apiv3_filtering/python_request_example.py), [Ruby](apiv3_filtering/ruby_request_example.rb), [PHP](apiv3_filtering/php_request_example.php).

## Callbacks

You can setup a webhook to receive callbacks anytime an event is triggered on a student session. The list of events can be found [here](https://docs.proctorexam.com/events/eventdoc.html)

1. Input your callback URL in the Advanced Settings of the institute. You need to set this up per institute, but the callback URL can be the same for all institutes.
    - Alternatively, you can use the `POST /institutes` and `PATCH /institutes/:id` API to set the callback URL for an institute
2. When an [event](https://docs.proctorexam.com/events/eventdoc.html) occurs, your webhook will be called with a POST request with the `event` object in the body of the request
    ```json
    {
        "event": {
            "id": 905652,
            "name": "start_sharing_screen",
            "created_at": "2023-06-29T17:26:08.543+02:00",
            "updated_at": "2023-06-29T17:26:08.543+02:00",
            "commentary": "",
            "participations": [
            {
                "id": 930735,
                "participatable_id": 25412,
                "participatable_type": "StudentSession",
                "event_id": 905652,
                "created_at": "2023-06-29T17:26:08.549+02:00",
                "updated_at": "2023-06-29T17:26:08.549+02:00"
            }
            ]
        }
    }
    ```
    - The `event --> name` string tells you the type of event, in the example above `start_sharing_screen`.
    - The `event --> participations` contains a list of related objects. The `participation` with `"participatable_type": "StudentSession"` is the concerned student session. Use
    the `id` of the `participation` to map agains a particular student session, in the above example `25412` (e.g. by doing a `GET /student_sessions/25412` to fetch all information
    for student session `25412`).
    - You will also see `participations` with `participatable_type` of `"User"`, for example when a user reviews the student session. In that case you can fetch the corresponding user
    with the [`GET /institutes/:id/users/:id`](https://docs.proctorexam.com/v3/apidoc/users/show.html) API. Note that you need to know the id of the institute. It's therefore recommended to setup a different webhook URL per institute, in order to
    capture the institute id in the webhook URL itself.
